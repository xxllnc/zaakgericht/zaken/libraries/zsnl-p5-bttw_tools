#!/usr/bin/perl -w

use Test::More;
use Test::Kwalitee qw[kwalitee_ok];

# strictness checks ignore re-imported strict from Moose
kwalitee_ok qw[
    -use_strict
];

done_testing;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the C<CONTRIBUTORS> file.

BTTW::Tools uses the EUPL license, for more information please have a look at the C<LICENSE> file.
