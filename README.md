# NAME

BTTW - Balls to the Wall utilities library

# DESCRIPTION

BTTW is a collection of utility libraries used by the [Zaaksysteem](https://metacpan.org/pod/Zaaksysteem) project.

See [BTTW::Tools](https://metacpan.org/pod/BTTW%3A%3ATools) for more info.

# COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the [CONTRIBUTORS](https://metacpan.org/pod/Zaaksysteem%3A%3ACONTRIBUTORS) file.

Zaaksysteem uses the EUPL license, for more information please have a look at the [LICENSE](https://metacpan.org/pod/Zaaksysteem%3A%3ALICENSE) file.
