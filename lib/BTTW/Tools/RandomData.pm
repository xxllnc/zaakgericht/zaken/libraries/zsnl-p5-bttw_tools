package BTTW::Tools::RandomData;
our $VERSION = '0.014';
use warnings;
use strict;

=head1 NAME

BTTW::Tools::RandomData - Tools for generating data

=head1 DESCRIPTION

This module provides methods to generate fake and/or random data used
for spoofing and/or faking data such as BSN numbers and KvK numbers.

=head1 SYNOPSIS

    use BTTW::Tools::RandomData qw(generate_bsn);

    my $fake_bsn  = generate_bsn();
    my $fake_kvk  = generate_kvk();
    my $fake_rsin = generate_rsin();

    my $uuid_v4   = generate_uuid_v4();

=cut

use autodie;

use feature ();
use Exporter qw(import);
use Carp qw(croak);
use Data::Random::NL qw(:all);

=head1 EXPORT_OK

=over

=item generate_bsn

=item generate_rsin

=item generate_kvk

=item generate_uuid_v4

=back

=cut

our @EXPORT_OK = (
    qw(
        generate_bsn
        generate_rsin
        generate_kvk
        generate_uuid_v4
    ),
);

=head1 EXPORT_TAGS

=over

=item :all

=item :fake

Get all the fake data methods

This includes: generate_bsn, generate_rsin, generate_kvk

=item :uuid

Get all the UUID data methods

This includes generate_uuid_v4

=back

=cut

our %EXPORT_TAGS = (
    all => \@EXPORT_OK,
    fake => [qw(generate_bsn generate_rsin generate_kvk)],
    uuid => [qw(generate_uuid_v4)],
);

use UUID::FFI;

=head2 generate_uuid_v4

Generate v4 UUID's

=cut

sub generate_uuid_v4 {
    return UUID::FFI->new_random() . "";
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS> file.

BTTW::Tools uses the EUPL license, for more information please have a look at the L<LICENSE> file.
