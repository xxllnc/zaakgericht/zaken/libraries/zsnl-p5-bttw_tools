package BTTW::Tools::Text::OOXML;
our $VERSION = '0.014';
use Moose;

use BTTW::Tools;

use Document::OOXML;

=head1 NAME

BTTW::Tools::Text::OOXML - Provide plaintext interface to Office Open XML files (docx)

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 file

=cut

has file => (
    is => 'rw',
    isa => 'Str',
    required => 1,
);

=head2 plaintext

=head3 Interface

=over 4

=item has_plaintext

Value predicate for this attribute.

=item build_plaintext

Our implementation of this attribute's value.

=back

=cut

has plaintext => (
    is => 'rw',
    isa => 'Str',
    predicate => 'has_plaintext',
    builder => 'build_plaintext'
);

=head1 BUILDERS

=head2 build_plaintext

Default value builder implementation for the L</plaintext> attribute.

=cut

sub build_plaintext {
    my $self = shift;

    my $doc = Document::OOXML->read_document($self->file);
    
    return join(" ", @{ $doc->extract_words });
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS> file.

BTTW::Tools uses the EUPL license, for more information please have a look at the L<LICENSE> file.
