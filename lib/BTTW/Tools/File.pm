package BTTW::Tools::File;
our $VERSION = '0.014';
use warnings;
use strict;

use autodie;
use v5.14;

use feature ();
use Exporter qw(import);
use BTTW::Tools;
use File::ArchivableFormats;
use List::Util qw(none any);

our @EXPORT_OK = (
    qw(
        sanitize_filename
        get_file_extension
        fix_file_extension
        fix_file_extension_for_fh
    ),
);

our %EXPORT_TAGS = (
    all => \@EXPORT_OK
);

=head1 NAME

BTTW::Tools::File - Basic utility functions for file (name) handling

=head1 SYNOPSIS

    package SomePackage::SomePart;
    use BTTW::Tools::File qw(:all);

    print sanitize_filename("blabla:bla.doc");
    # prints "blablabla.doc"

    print get_file_extension("/tmp/bla.txt");
    # prints ".txt"

=head1 METHODS


=head2 sanitize_filename

Replace all "scary" characters in a user-supplied filename with "_".

Disallowed characters are:

=over

=item * '<', '>', '"' and '&'

Because of their special meaning in HTML, and because Windows doesn't allow
some of them.

=item * ":"

For weird path effects on Macs and as "drive letter separator" on Windows.

=item * "?", "|" and "*"

Because Windows doesn't allow these in file names.

=item * Control characters

All characters defined as "control characters" in Unicode will be removed.

These won't be replaced by "_", but completely removed.

=back

Path separators ("/" and "\") are allowed, but only the last segment is returned.

Windows sometimes uploads files with the full path as a filename
("Z:\Foo\Bar\Baz.docx"), in which case we only want the filename part.

=cut

sub sanitize_filename {
    my $filename = shift;

    throw('sanitize_filename', "Need a filename") unless defined $filename;

    $filename =~ s#[<>&":|?*]#_#g;

    # Remove all unprintable (= control) characters
    $filename =~ s#[^[:print:]]##g;

    # Reserved names in Windows. Add an underscore to fix.
    my @invalidWindowsNames = qw(con nul prn);
    for (1 .. 9) {
        push @invalidWindowsNames, ("com$_", "lpt$_");
    }

    if (grep {$_ eq $filename} @invalidWindowsNames) {
        $filename .= "_";
    }

    # Strip file path (DOS or UNIX)
    return (split m{[\\/]}, $filename)[-1];
}

=head2 get_file_extension

Central utility method to determine the file extension based on a file path.
Looks for a dot (.) and 1 to 5 characters at the end of the given path.

E.g. /tmp/something/sub/filename.ext => yields '.ext'

Return file extension including the dot (.)

=cut

sub get_file_extension {
    my ($path) = @_;

    my ($extension) = $path =~ qr/(\.[0-9A-Za-z]{1,5}$)/;

    return $extension;
}

=head2 fix_file_extension

Rename a file if it doesn't have the correct file extension

=cut

sub fix_file_extension {
    my ($path) = @_;

    my $af   = File::ArchivableFormats->new();
    my $info = $af->identify_from_path($path);
    return _fix_filename($path, $info);
}

=head2 fix_file_extension_for_fh

    fix_file_extension_for_fh($filehandle, '/path/to/file');

Rename a file if it doesn't have the correct file extension
for a file handle. For when you have a File::Temp handle and
you know the target filename.

=cut

sub fix_file_extension_for_fh {
    my ($fh, $filename) = @_;

    my $af   = File::ArchivableFormats->new();
    my $info = $af->identify_from_path("$fh");
    return _fix_filename($filename, $info);
}

sub _fix_filename {
    my ($path, $info) = @_;

    my $extension = lc(get_file_extension($path)//'');
    if ($info->{DANS}{archivable}) {
        my @allowed = @{$info->{DANS}{allowed_extensions}};
        if (none {$extension eq $_} @allowed) {
            if (my $ext = $info->{DANS}{preferred_extension}) {
                $path .= $ext;
            }
            else {
                $path .= $allowed[0];
            }
        }
        return $path;
    }

    if (any { $_ eq $info->{mime_type} } qw(application/vnd.ms-outlook application/x-ole-storage message/rfc822)) {
        if ($extension eq ".msg") {
            return $path;
        }
        return "$path.msg";
    }

    return $path;

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS> file.

BTTW::Tools uses the EUPL license, for more information please have a look at the L<LICENSE> file.
