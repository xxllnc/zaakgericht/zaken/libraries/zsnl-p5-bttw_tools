package BTTW::Types::Core;
our $VERSION = '0.014';
use Data::Validate::UUID qw[is_uuid];
use DateTime::Format::ISO8601;
use DateTime::Format::Duration::ISO8601;
use Email::Valid;

use MooseX::Types::Moose qw[:all];

use MooseX::Types -declare => [qw[
    NonEmptyStr
    UUID
    SequenceNumber
    EmailAddress
    Timestamp
    Datestamp
    Duration
]];

=head1 NAME

BTTW::Types::Core - Core BTTW Moose TypeConstraints

=head1 DESCRIPTION

This package contains a collection of L<Moose::Meta::TypeConstraints>,
exposed via L<MooseX::Types>.

The provided types are considered 'core' insofar they are used often in
L<Zaaksysteem> projects.

=head1 TYPES

=head2 UUID

Validates whether the provided C<Str> is a valid C<UUID> (of any version and
variant).

=cut

subtype UUID,
    as Str,
    where { defined $_[0] && is_uuid($_[0]) },
    message { sprintf('"%s" is not a valid UUID', $_) };

=head2 NonEmptyStr

=cut

subtype NonEmptyStr,
    as Str,
    where { length },
    message { sprintf('"%s" is not a non-empty string', $_) };

=head2 SequenceNumber

=cut

subtype SequenceNumber,
    as Int,
    where { $_ > 0 },
    message { sprintf('"%s" is not a sequence number (> 0)', $_) };

=head2 EmailAddress

=cut

subtype EmailAddress,
    as NonEmptyStr,
    where { Email::Valid->address(-address => $_, -allow_ip => 0) },
    message { sprintf('"%s" is not a valid e-mail address', $_) };

=head2 Timestamp

=cut

class_type Timestamp, { class => 'DateTime' };

coerce Timestamp,
    from Str,
    via { DateTime::Format::ISO8601->parse_datetime($_) };

=head2 Datestamp

=cut

class_type Datestamp, { class => 'DateTime' };

coerce Datestamp,
    from Timestamp,
    via { $_->truncate(to => 'day') };

=head2 Duration

=cut

class_type Duration, { class => 'DateTime::Duration' };

coerce Duration,
    from Str,
    via { DateTime::Duration::Format::ISO8601->parse_duration($_) };

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS> file.

BTTW::Tools uses the EUPL license, for more information please have a look at the L<LICENSE> file.
