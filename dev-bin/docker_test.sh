#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../"

cd $DIR;

if [ "$(docker images -a|grep registry.gitlab.com/zaaksysteem/bttw-tools)" != "" ]; then
    echo "Found existing docker image, no build"
else
    echo "Did not find existing image, building"
    docker build -t registry.gitlab.com/zaaksysteem/bttw-tools .
fi

if [ "$1" == "" ]; then
    docker run --rm -w /opt/bttw-tools -v $DIR:/opt/bttw-tools \
        registry.gitlab.com/zaaksysteem/bttw-tools \
        prove -v -l t
else
    docker run --rm -v $DIR:/opt/bttw-tools \
        registry.gitlab.com/zaaksysteem/bttw-tools \
        prove -v -l $@
fi
