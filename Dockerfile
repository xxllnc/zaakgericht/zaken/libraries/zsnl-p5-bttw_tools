FROM perl:5-bullseye

ENV NO_NETWORK_TESTING=1 \
    DEBIAN_FRONTEND=noninteractive

COPY dev-bin/cpanm /usr/local/bin/docker-cpanm

# libmagic-dev: Required by File::ArchivableFormat' File::LibMagic dependency
# poppler-utils: Required for pdftotext
RUN apt-get update && apt-get upgrade -y \
    && apt-get autoremove --purge -yqq \
    && apt-get clean \
    && apt-get install --no-install-recommends -y \
        libmagic-dev \
        poppler-utils \
    && rm -rf /var/lib/apt/lists/* /var/cache/apt/* ~/.cpanm

# Install dependency
RUN docker-cpanm https://gitlab.com/waterkip/file-archivableformats.git@v1.5.3
RUN docker-cpanm File::ShareDir::Install
COPY cpanfile .
RUN docker-cpanm --installdeps .
COPY . .
RUN docker-cpanm . && make clean
